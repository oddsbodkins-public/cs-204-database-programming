CREATE TABLE author (
	author_id			serial PRIMARY KEY,
    author_first_name 	VARCHAR (100) NOT NULL,
    author_last_name 	VARCHAR (100) NOT NULL,
    author_nationality 	VARCHAR (30) NOT NULL
);

CREATE TABLE book (
	book_id			serial PRIMARY KEY,
    book_title 		VARCHAR (200) NOT NULL,
    book_author		INT NOT NULL,
    genre			VARCHAR (50) NOT NULL,
	FOREIGN KEY (book_author) REFERENCES author (author_id)
);

CREATE TABLE client (
	client_id				serial PRIMARY KEY,
    client_first_name		VARCHAR (100) NOT NULL,
    client_last_name		VARCHAR (100) NULL,
    client_date_of_birth	INT NOT NULL,
    client_occupation		VARCHAR (50) NOT NULL
);

CREATE TABLE borrower (
	borrow_id			serial PRIMARY KEY,
    client_id 			INT NOT NULL,
    book_id				INT NOT NULL,
    borrow_date			DATE NOT NULL,
	FOREIGN KEY (client_id) REFERENCES client (client_id),
	FOREIGN KEY (book_id) REFERENCES book (book_id)
);

----
-- DROP TABLE author CASCADE;
-- DROP TABLE book CASCADE;
-- DROP TABLE borrower CASCADE;
-- DROP TABLE client CASCADE;



-- DROP SCHEMA public CASCADE;
-- CREATE SCHEMA public;

-- If you are using PostgreSQL 9.3 or greater, you may also need to restore the default grants.

-- GRANT ALL ON SCHEMA public TO postgres;
-- GRANT ALL ON SCHEMA public TO public;

-- Question 1
SELECT * FROM client;

---Question 2
SELECT client_first_name, client_last_name, AGE(TO_DATE(TO_CHAR(client_date_of_birth, '0000'), 'YYYY')) AS client_age, client_occupation FROM client;

---Question 3
SELECT client_first_name, client_last_name FROM client AS c INNER JOIN borrower AS b ON c.client_id = b.client_id WHERE borrow_date BETWEEN '2018-03-01' AND '2018-03-31';

---Question 4
SELECT author.author_first_name, author.author_last_name, borrower.book_id, COUNT(borrower.book_id) FROM borrower INNER JOIN book ON borrower.book_id = book.book_id INNER JOIN author ON author.author_id = book.book_author WHERE borrow_date BETWEEN '2017-01-01' AND '2017-12-31' GROUP BY borrower.book_id, author.author_first_name, author.author_last_name ORDER BY COUNT(borrower.book_id) DESC LIMIT 5;

---Question 5
SELECT author.author_nationality FROM borrower INNER JOIN book ON borrower.book_id = book.book_id INNER JOIN author ON book.book_author = author.author_id WHERE borrower.borrow_date BETWEEN '2015-01-01' AND '2017-12-31' GROUP BY author.author_nationality LIMIT 5;

---Question 6
SELECT book.book_title, borrower.book_id, COUNT(borrower.book_id) FROM borrower INNER JOIN book ON book.book_id = borrower.book_id WHERE borrower.borrow_date BETWEEN '2015-01-01' AND '2017-12-31' GROUP BY borrower.book_id, book.book_title ORDER BY COUNT(borrower.book_id) DESC;

---Question 7
SELECT book.genre, COUNT(book.genre) FROM client INNER JOIN borrower ON client.client_id = borrower.client_id INNER JOIN book ON borrower.book_id = book.book_id WHERE client.client_date_of_birth >= 1970 AND client.client_date_of_birth <= 1980 GROUP BY book.genre ORDER BY COUNT(book.genre) DESC;

---Question 8
SELECT client.client_occupation, COUNT(client.client_occupation) FROM borrower INNER JOIN client ON borrower.client_id = client.client_id WHERE borrower.borrow_date BETWEEN '2016-01-01' AND '2016-12-31' GROUP BY client.client_occupation ORDER BY COUNT(client.client_occupation) DESC LIMIT 5;

---Question 9
--- Create temporary views / tables for calculations 
---CREATE VIEW qs9_step1 AS SELECT borrower.client_id, client.client_occupation, COUNT(borrower.client_id) AS rentals FROM borrower INNER JOIN client ON borrower.client_id = client.client_id GROUP BY borrower.client_id, client.client_occupation ORDER BY rentals DESC;
---CREATE VIEW qs9_step2 AS SELECT client_occupation, SUM(rentals) AS sum_of_rentals, COUNT(client_occupation) AS count_of_occupation FROM qs9_step1 GROUP BY client_occupation ORDER BY count_of_occupation DESC;
---SELECT client_occupation, ROUND (sum_of_rentals / count_of_occupation, 2) AS average FROM qs9_step2;

---Question 9

--- List the total number of checkouts by client id and occupation
WITH qs9_step1 AS (
	SELECT borrower.client_id, client.client_occupation, COUNT(borrower.client_id) AS rentals
	FROM borrower
	INNER JOIN client ON borrower.client_id = client.client_id
	GROUP BY borrower.client_id, client.client_occupation
	ORDER BY rentals DESC
), qs9_step2 AS ( --Sum the total checkouts grouped by occupation (numerator) and the number of clients with the occupation (denominator)
	SELECT client_occupation, SUM(rentals) AS sum_of_rentals, COUNT(client_occupation) AS count_of_occupation
	FROM qs9_step1
	GROUP BY client_occupation
	ORDER BY count_of_occupation DESC
) 
SELECT client_occupation, ROUND (sum_of_rentals / count_of_occupation, 2) AS average FROM qs9_step2; -- Divide the numbers and round to 2 decimal points


---Question 10

CREATE VIEW popular_book_titles AS
WITH qs10_step1 AS ( -- Count the number of times a book has been borrowed
	SELECT borrower.book_id, book.book_title, COUNT(borrower.book_id)
	FROM borrower
	INNER JOIN book ON borrower.book_id = book.book_id
	GROUP BY borrower.book_id, book.book_title
	ORDER BY COUNT(borrower.book_id) DESC
), qs10_step2 AS ( -- Calculate what 20% of clients is, could be precomputed, but I assume that the number of clients changes often
	SELECT ROUND(COUNT(client_id) * 0.2, 0) AS twenty_percent
	FROM client
)
SELECT * FROM qs10_step1, qs10_step2 WHERE count > qs10_step2.twenty_percent;

-- To test the view
SELECT * FROM popular_book_titles;

CREATE VIEW qs10_step1 AS SELECT borrower.book_id, book.book_title, COUNT(borrower.book_id) FROM borrower INNER JOIN book ON borrower.book_id = book.book_id GROUP BY borrower.book_id, book.book_title ORDER BY COUNT(borrower.book_id) DESC;
CREATE VIEW qs10_step2 AS SELECT ROUND(COUNT(client_id) * 0.2, 0) AS twenty_percent FROM client; -- Calculate what 20% of clients is, could be precomputed, but I assume that the number of clients changes often
CREATE VIEW popular_book_titles AS SELECT * FROM qs10_step1, qs10_step2 WHERE count > qs10_step2.twenty_percent;
SELECT * FROM popular_book_titles;


---Question 11
CREATE VIEW qs11_step1 AS SELECT borrow_id, borrow_date, TO_CHAR(borrow_date, 'Month') AS MONTH FROM borrower WHERE borrow_date BETWEEN '2017-01-01' AND '2017-12-31' GROUP BY month, borrow_id ORDER BY borrow_date;
SELECT month, COUNT(month) FROM qs11_step1 GROUP BY month ORDER BY count DESC;

---Question 12
CREATE VIEW qs12_step1 AS SELECT COUNT(borrower.client_id), client.client_date_of_birth from borrower INNER JOIN client ON borrower.client_id = client.client_id GROUP BY borrower.client_id, client.client_date_of_birth;
CREATE VIEW qs12_step2 AS SELECT SUM(count), client_date_of_birth FROM qs12_step1 GROUP BY client_date_of_birth ORDER BY client_date_of_birth;
CREATE VIEW qs12_step3 AS SELECT COUNT(client_date_of_birth), client_date_of_birth FROM qs12_step1 GROUP BY client_date_of_birth ORDER BY client_date_of_birth;
SELECT ROUND(qs12_step2.sum / qs12_step3.count, 2) AS average, qs12_step2.client_date_of_birth FROM qs12_step2 INNER JOIN qs12_step3 ON qs12_step2.client_date_of_birth = qs12_step3.client_date_of_birth;

---Question 13
---Two parts

---Oldest clients:
SELECT * FROM client ORDER BY client_date_of_birth LIMIT 1;

---Youngest clients:
SELECT * FROM client ORDER BY client_date_of_birth DESC LIMIT 1;

---Question 14
---Note there is a small error on the assignment description. In listing for the Book table, BookID 26: the genre is listed as 'well being'.
--- The 'w' is lower case unlike the others with a capital 'W' (Ex: "Well being"). In Postgres / Linux case sensitivity matters and it counts as a different genre in my testing.
--- I corrected it on my machine.
--- Ex: "UPDATE book SET genre = 'Well being' WHERE book_id = 26;"

CREATE VIEW qs14_step1 AS SELECT book_author, COUNT(book_id) FROM book GROUP BY book_author HAVING COUNT(book_id) > 1 ORDER BY COUNT(book_id) DESC;
CREATE VIEW qs14_step2 AS SELECT DISTINCT qs14_step1.book_author, genre FROM book INNER JOIN qs14_step1 ON book.book_author = qs14_step1.book_author ORDER BY book_author;
SELECT COUNT(qs14_step2.book_author), qs14_step2.book_author, author.author_first_name, author.author_last_name FROM qs14_step2 INNER JOIN author ON qs14_step2.book_author = author.author_id GROUP BY qs14_step2.book_author, author.author_first_name, author.author_last_name HAVING COUNT(qs14_step2.book_author) > 1;
