# Run on Debian 10

sudo apt update
sudo apt install postgresql
sudo -iu postgres
createuser --interactive # username: cs204
createdb library_assignment -O cs204
psql -d library_assignment
# Change role password
# ALTER ROLE cs204 WITH PASSWORD 'SDC assignment';

psql -d library_assignment -f cs204-database-programming-assignment.txt.sql postgres

# SQL code

# Import CSV files
COPY author FROM '/home/user/Documents/college-classes/cs-204/author_table_data.csv' WITH (FORMAT csv);
COPY book FROM '/home/user/Documents/college-classes/cs-204/book_table_data.csv' WITH (FORMAT csv);
COPY client FROM '/home/user/Documents/college-classes/cs-204/client_table_data.csv' WITH (FORMAT csv);
COPY borrower FROM '/home/user/Documents/college-classes/cs-204/borrower_table_data.csv' WITH (FORMAT csv);
