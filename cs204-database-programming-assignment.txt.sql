--- Create all the tables in PostgreSQL
--- PostgreSQL Version 13.4, run on Debian Linux (Debian 13.4-0+deb11u1)

CREATE TABLE author (
	author_id			serial PRIMARY KEY,
    author_first_name 	VARCHAR (100) NOT NULL,
    author_last_name 	VARCHAR (100) NOT NULL,
    author_nationality 	VARCHAR (30) NOT NULL
);

CREATE TABLE book (
	book_id			serial PRIMARY KEY,
    book_title 		VARCHAR (200) NOT NULL,
    book_author		INT NOT NULL,
    genre			VARCHAR (50) NOT NULL,
	FOREIGN KEY (book_author) REFERENCES author (author_id)
);

CREATE TABLE client (
	client_id				serial PRIMARY KEY,
    client_first_name		VARCHAR (100) NOT NULL,
    client_last_name		VARCHAR (100) NULL,
    client_date_of_birth	INT NOT NULL,
    client_occupation		VARCHAR (50) NOT NULL
);

CREATE TABLE borrower (
	borrow_id			serial PRIMARY KEY,
    client_id 			INT NOT NULL,
    book_id				INT NOT NULL,
    borrow_date			DATE NOT NULL,
	FOREIGN KEY (client_id) REFERENCES client (client_id),
	FOREIGN KEY (book_id) REFERENCES book (book_id)
);

---- I saved the table data as a CSV file and imported them

# Import CSV files
COPY author FROM '/home/user/Documents/wgu-college-classes/cs-204/author_table_data.csv' WITH (FORMAT csv);
COPY book FROM '/home/user/Documents/wgu-college-classes/cs-204/book_table_data.csv' WITH (FORMAT csv);
COPY client FROM '/home/user/Documents/wgu-college-classes/cs-204/client_table_data.csv' WITH (FORMAT csv);
COPY borrower FROM '/home/user/Documents/wgu-college-classes/cs-204/borrower_table_data.csv' WITH (FORMAT csv);

--- Answers to Questions

-- Question 1
SELECT * FROM client;

---Question 2

-- Calculate the age from the current year
SELECT client_first_name, client_last_name, AGE(TO_DATE(TO_CHAR(client_date_of_birth, '0000'), 'YYYY')) AS client_age, client_occupation FROM client;

---Question 3

-- Only keep borrowers from the beginning to the end of March 2018
SELECT client_first_name, client_last_name FROM client AS c INNER JOIN borrower AS b ON c.client_id = b.client_id WHERE borrow_date BETWEEN '2018-03-01' AND '2018-03-31';

---Question 4

--- Keep the borrowers from the beginning of the year to the end, count the number of checkouts and sort by the top
SELECT author.author_first_name, author.author_last_name, borrower.book_id, COUNT(borrower.book_id) FROM borrower INNER JOIN book ON borrower.book_id = book.book_id INNER JOIN author ON author.author_id = book.book_author WHERE borrow_date BETWEEN '2017-01-01' AND '2017-12-31' GROUP BY borrower.book_id, author.author_first_name, author.author_last_name ORDER BY COUNT(borrower.book_id) DESC LIMIT 5;

---Question 5

--- Keep the borrowers from the beginning of 2015 to the end of 2017, group by author nationality
SELECT author.author_nationality FROM borrower INNER JOIN book ON borrower.book_id = book.book_id INNER JOIN author ON book.book_author = author.author_id WHERE borrower.borrow_date BETWEEN '2015-01-01' AND '2017-12-31' GROUP BY author.author_nationality LIMIT 5;

---Question 6

--- Keep the borrowers from the beginning of 2015 to the end of 2017, list the book that was borrowed the most
SELECT book.book_title, borrower.book_id, COUNT(borrower.book_id) FROM borrower INNER JOIN book ON book.book_id = borrower.book_id WHERE borrower.borrow_date BETWEEN '2015-01-01' AND '2017-12-31' GROUP BY borrower.book_id, book.book_title ORDER BY COUNT(borrower.book_id) DESC;

---Question 7

--- Keep the clients that were born between 1970 and 1980, list the genres with the number of checkouts
SELECT book.genre, COUNT(book.genre) FROM client INNER JOIN borrower ON client.client_id = borrower.client_id INNER JOIN book ON borrower.book_id = book.book_id WHERE client.client_date_of_birth >= 1970 AND client.client_date_of_birth <= 1980 GROUP BY book.genre ORDER BY COUNT(book.genre) DESC;

---Question 8

--- Keep the borrows from the beginning of 2016 to the end of 2016, and then the most popular book in that timeframe
SELECT client.client_occupation, COUNT(client.client_occupation) FROM borrower INNER JOIN client ON borrower.client_id = client.client_id WHERE borrower.borrow_date BETWEEN '2016-01-01' AND '2016-12-31' GROUP BY client.client_occupation ORDER BY COUNT(client.client_occupation) DESC LIMIT 5;

---Question 9

--- List the total number of checkouts by client id and occupation
WITH qs9_step1 AS (
	SELECT borrower.client_id, client.client_occupation, COUNT(borrower.client_id) AS rentals
	FROM borrower
	INNER JOIN client ON borrower.client_id = client.client_id
	GROUP BY borrower.client_id, client.client_occupation
	ORDER BY rentals DESC
), qs9_step2 AS ( --Sum the total checkouts grouped by occupation (numerator) and the number of clients with the occupation (denominator)
	SELECT client_occupation, SUM(rentals) AS sum_of_rentals, COUNT(client_occupation) AS count_of_occupation
	FROM qs9_step1
	GROUP BY client_occupation
	ORDER BY count_of_occupation DESC
) 
SELECT client_occupation, ROUND (sum_of_rentals / count_of_occupation, 2) AS average FROM qs9_step2; -- Divide the numbers and round to 2 decimal points


---Question 10

CREATE VIEW popular_book_titles AS
WITH qs10_step1 AS ( -- Count the number of times a book has been borrowed
	SELECT borrower.book_id, book.book_title, COUNT(borrower.book_id)
	FROM borrower
	INNER JOIN book ON borrower.book_id = book.book_id
	GROUP BY borrower.book_id, book.book_title
	ORDER BY COUNT(borrower.book_id) DESC
), qs10_step2 AS ( -- Calculate what 20% of clients is, could be precomputed, but I assume that the number of clients changes often
	SELECT ROUND(COUNT(client_id) * 0.2, 0) AS twenty_percent
	FROM client
)
SELECT * FROM qs10_step1, qs10_step2 WHERE count > qs10_step2.twenty_percent;

-- To test the view
SELECT * FROM popular_book_titles;

---Question 11

WITH qs11_step1 AS ( -- Get the total number of checkouts for each month
	SELECT borrow_id, borrow_date, TO_CHAR(borrow_date, 'Month') AS MONTH
	FROM borrower
	WHERE borrow_date BETWEEN '2017-01-01' AND '2017-12-31'
	GROUP BY month, borrow_id
	ORDER BY borrow_date
) -- Order by highest number of borrows
SELECT month, COUNT(month) FROM qs11_step1 GROUP BY month ORDER BY count DESC;

---Question 12

WITH qs12_step1 AS ( -- Count the number of checkouts by each client with his birthyear
	SELECT COUNT(borrower.client_id), client.client_date_of_birth
	FROM borrower
	INNER JOIN client ON borrower.client_id = client.client_id
	GROUP BY borrower.client_id, client.client_date_of_birth
), qs12_step2 AS ( -- Sum the number of checkouts by each client with matching birthyear (numerator for average)
	SELECT SUM(count), client_date_of_birth
	FROM qs12_step1
	GROUP BY client_date_of_birth
	ORDER BY client_date_of_birth
), qs12_step3 AS ( -- Count the number of clients that share a birthyear, (denominator for average) 
	SELECT COUNT(client_date_of_birth), client_date_of_birth
	FROM qs12_step1
	GROUP BY client_date_of_birth
	ORDER BY client_date_of_birth
) -- Calculate the average number of borrows
SELECT ROUND(qs12_step2.sum / qs12_step3.count, 2) AS average, qs12_step2.client_date_of_birth FROM qs12_step2 INNER JOIN qs12_step3 ON qs12_step2.client_date_of_birth = qs12_step3.client_date_of_birth;

---Question 13
---Two parts

---Oldest clients:
SELECT * FROM client ORDER BY client_date_of_birth LIMIT 1;

---Youngest clients:
SELECT * FROM client ORDER BY client_date_of_birth DESC LIMIT 1;

---Question 14

--- Note, there is a small error on the assignment description. In listing for the Book table, BookID 26: the genre is listed as 'well being'.
--- The 'w' is lower case unlike the others with a capital 'W' (Ex: "Well being"). In Postgres / Linux case sensitivity matters and it counts as a different genre in my testing.
--- I corrected it on my machine.
--- Ex: "UPDATE book SET genre = 'Well being' WHERE book_id = 26;"

WITH qs14_step1 AS ( -- Count the number of books each author wrote, only keep those with 2 or more
	SELECT book_author, COUNT(book_id)
	FROM book
	GROUP BY book_author HAVING COUNT(book_id) > 1
	ORDER BY COUNT(book_id) DESC
), qs14_step2 AS ( -- Of those remaining, what genres have they written? Are there are different genres (DISTINCT)
	SELECT DISTINCT qs14_step1.book_author, genre
	FROM book
	INNER JOIN qs14_step1 ON book.book_author = qs14_step1.book_author
	ORDER BY book_author
)
SELECT COUNT(qs14_step2.book_author), qs14_step2.book_author, author.author_first_name, author.author_last_name FROM qs14_step2 INNER JOIN author ON qs14_step2.book_author = author.author_id GROUP BY qs14_step2.book_author, author.author_first_name, author.author_last_name HAVING COUNT(qs14_step2.book_author) > 1;

--- End of SQL code, results are below


