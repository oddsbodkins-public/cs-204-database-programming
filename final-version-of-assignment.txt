--- 21-Oct-2021
--- Computer Science 204: Database Programming
--- Database Programming - Assignment: Creating & Manipulating a Database 

--- I used PostgreSQL. https://www.postgresql.org/
--- I hope that is not a problem. I only saw Oracle, MS SQL Server, and MySQL covered in the course, but I also have experience with PostgreSQL. 
--- PostgreSQL Version 13.4, run on Debian Linux (Debian 13.4-0+deb11u1)

--- SQL code

--- Create all the tables in PostgreSQL

CREATE TABLE author (
	author_id			serial PRIMARY KEY,
    author_first_name 	VARCHAR (100) NOT NULL,
    author_last_name 	VARCHAR (100) NOT NULL,
    author_nationality 	VARCHAR (30) NOT NULL
);

CREATE TABLE book (
	book_id			serial PRIMARY KEY,
    book_title 		VARCHAR (200) NOT NULL,
    book_author		INT NOT NULL,
    genre			VARCHAR (50) NOT NULL,
	FOREIGN KEY (book_author) REFERENCES author (author_id)
);

CREATE TABLE client (
	client_id				serial PRIMARY KEY,
    client_first_name		VARCHAR (100) NOT NULL,
    client_last_name		VARCHAR (100) NULL,
    client_date_of_birth	INT NOT NULL,
    client_occupation		VARCHAR (50) NOT NULL
);

CREATE TABLE borrower (
	borrow_id			serial PRIMARY KEY,
    client_id 			INT NOT NULL,
    book_id				INT NOT NULL,
    borrow_date			DATE NOT NULL,
	FOREIGN KEY (client_id) REFERENCES client (client_id),
	FOREIGN KEY (book_id) REFERENCES book (book_id)
);

---- I saved the table data as CSV files and imported them

# Import CSV files
COPY author FROM '/home/user/Documents/college-classes/cs-204/author_table_data.csv' WITH (FORMAT csv);
COPY book FROM '/home/user/Documents/college-classes/cs-204/book_table_data.csv' WITH (FORMAT csv);
COPY client FROM '/home/user/Documents/college-classes/cs-204/client_table_data.csv' WITH (FORMAT csv);
COPY borrower FROM '/home/user/Documents/college-classes/cs-204/borrower_table_data.csv' WITH (FORMAT csv);

--- Answers to Questions

-- Question 1
SELECT * FROM client;

---Question 2

-- Calculate the age from the current year
SELECT client_first_name, client_last_name, AGE(TO_DATE(TO_CHAR(client_date_of_birth, '0000'), 'YYYY')) AS client_age, client_occupation FROM client;

---Question 3

-- Only keep borrowers from the beginning to the end of March 2018
SELECT client_first_name, client_last_name FROM client AS c INNER JOIN borrower AS b ON c.client_id = b.client_id WHERE borrow_date BETWEEN '2018-03-01' AND '2018-03-31';

---Question 4

--- Keep the borrowers from the beginning of the year to the end, count the number of checkouts and sort by the top
SELECT author.author_first_name, author.author_last_name, borrower.book_id, COUNT(borrower.book_id) FROM borrower INNER JOIN book ON borrower.book_id = book.book_id INNER JOIN author ON author.author_id = book.book_author WHERE borrow_date BETWEEN '2017-01-01' AND '2017-12-31' GROUP BY borrower.book_id, author.author_first_name, author.author_last_name ORDER BY COUNT(borrower.book_id) DESC LIMIT 5;

---Question 5

--- Keep the borrowers from the beginning of 2015 to the end of 2017, group by author nationality
SELECT author.author_nationality FROM borrower INNER JOIN book ON borrower.book_id = book.book_id INNER JOIN author ON book.book_author = author.author_id WHERE borrower.borrow_date BETWEEN '2015-01-01' AND '2017-12-31' GROUP BY author.author_nationality LIMIT 5;

---Question 6

--- Keep the borrowers from the beginning of 2015 to the end of 2017, list the book that was borrowed the most
SELECT book.book_title, borrower.book_id, COUNT(borrower.book_id) FROM borrower INNER JOIN book ON book.book_id = borrower.book_id WHERE borrower.borrow_date BETWEEN '2015-01-01' AND '2017-12-31' GROUP BY borrower.book_id, book.book_title ORDER BY COUNT(borrower.book_id) DESC;

---Question 7

--- Keep the clients that were born between 1970 and 1980, list the genres with the number of checkouts
SELECT book.genre, COUNT(book.genre) FROM client INNER JOIN borrower ON client.client_id = borrower.client_id INNER JOIN book ON borrower.book_id = book.book_id WHERE client.client_date_of_birth >= 1970 AND client.client_date_of_birth <= 1980 GROUP BY book.genre ORDER BY COUNT(book.genre) DESC;

---Question 8

--- Keep the borrows from the beginning of 2016 to the end of 2016, and then the most popular book in that timeframe
SELECT client.client_occupation, COUNT(client.client_occupation) FROM borrower INNER JOIN client ON borrower.client_id = client.client_id WHERE borrower.borrow_date BETWEEN '2016-01-01' AND '2016-12-31' GROUP BY client.client_occupation ORDER BY COUNT(client.client_occupation) DESC LIMIT 5;

---Question 9

--- List the total number of checkouts by client id and occupation
WITH qs9_step1 AS (
	SELECT borrower.client_id, client.client_occupation, COUNT(borrower.client_id) AS rentals
	FROM borrower
	INNER JOIN client ON borrower.client_id = client.client_id
	GROUP BY borrower.client_id, client.client_occupation
	ORDER BY rentals DESC
), qs9_step2 AS ( --Sum the total checkouts grouped by occupation (numerator) and the number of clients with the occupation (denominator)
	SELECT client_occupation, SUM(rentals) AS sum_of_rentals, COUNT(client_occupation) AS count_of_occupation
	FROM qs9_step1
	GROUP BY client_occupation
	ORDER BY count_of_occupation DESC
) 
SELECT client_occupation, ROUND (sum_of_rentals / count_of_occupation, 2) AS average FROM qs9_step2; -- Divide the numbers and round to 2 decimal points


---Question 10

CREATE VIEW popular_book_titles AS
WITH qs10_step1 AS ( -- Count the number of times a book has been borrowed
	SELECT borrower.book_id, book.book_title, COUNT(borrower.book_id)
	FROM borrower
	INNER JOIN book ON borrower.book_id = book.book_id
	GROUP BY borrower.book_id, book.book_title
	ORDER BY COUNT(borrower.book_id) DESC
), qs10_step2 AS ( -- Calculate what 20% of clients is, could be precomputed, but I assume that the number of clients changes often
	SELECT ROUND(COUNT(client_id) * 0.2, 0) AS twenty_percent
	FROM client
)
SELECT * FROM qs10_step1, qs10_step2 WHERE count > qs10_step2.twenty_percent;

-- To test the view
SELECT * FROM popular_book_titles;

---Question 11

WITH qs11_step1 AS ( -- Get the total number of checkouts for each month
	SELECT borrow_id, borrow_date, TO_CHAR(borrow_date, 'Month') AS MONTH
	FROM borrower
	WHERE borrow_date BETWEEN '2017-01-01' AND '2017-12-31'
	GROUP BY month, borrow_id
	ORDER BY borrow_date
) -- Order by highest number of borrows
SELECT month, COUNT(month) FROM qs11_step1 GROUP BY month ORDER BY count DESC;

---Question 12

WITH qs12_step1 AS ( -- Count the number of checkouts by each client with his birthyear
	SELECT COUNT(borrower.client_id), client.client_date_of_birth
	FROM borrower
	INNER JOIN client ON borrower.client_id = client.client_id
	GROUP BY borrower.client_id, client.client_date_of_birth
), qs12_step2 AS ( -- Sum the number of checkouts by each client with matching birthyear (numerator for average)
	SELECT SUM(count), client_date_of_birth
	FROM qs12_step1
	GROUP BY client_date_of_birth
	ORDER BY client_date_of_birth
), qs12_step3 AS ( -- Count the number of clients that share a birthyear, (denominator for average) 
	SELECT COUNT(client_date_of_birth), client_date_of_birth
	FROM qs12_step1
	GROUP BY client_date_of_birth
	ORDER BY client_date_of_birth
) -- Calculate the average number of borrows
SELECT ROUND(qs12_step2.sum / qs12_step3.count, 2) AS average, qs12_step2.client_date_of_birth FROM qs12_step2 INNER JOIN qs12_step3 ON qs12_step2.client_date_of_birth = qs12_step3.client_date_of_birth;

---Question 13
---Two parts

---Oldest clients:
SELECT * FROM client ORDER BY client_date_of_birth LIMIT 1;

---Youngest clients:
SELECT * FROM client ORDER BY client_date_of_birth DESC LIMIT 1;

---Question 14

--- Note, there is a small error on the assignment description. In listing for the Book table, BookID 26: the genre is listed as 'well being'.
--- The 'w' is lower case unlike the others with a capital 'W' (Ex: "Well being"). In Postgres / Linux case sensitivity matters and it counts as a different genre in my testing.
--- I corrected it on my machine.
--- Ex: "UPDATE book SET genre = 'Well being' WHERE book_id = 26;"

WITH qs14_step1 AS ( -- Count the number of books each author wrote, only keep those with 2 or more
	SELECT book_author, COUNT(book_id)
	FROM book
	GROUP BY book_author HAVING COUNT(book_id) > 1
	ORDER BY COUNT(book_id) DESC
), qs14_step2 AS ( -- Of those remaining, what genres have they written? Are there are different genres (DISTINCT)
	SELECT DISTINCT qs14_step1.book_author, genre
	FROM book
	INNER JOIN qs14_step1 ON book.book_author = qs14_step1.book_author
	ORDER BY book_author
)
SELECT COUNT(qs14_step2.book_author), qs14_step2.book_author, author.author_first_name, author.author_last_name FROM qs14_step2 INNER JOIN author ON qs14_step2.book_author = author.author_id GROUP BY qs14_step2.book_author, author.author_first_name, author.author_last_name HAVING COUNT(qs14_step2.book_author) > 1;

--- End of SQL code, query results are below

Query Results
-------------------

Question 1

client_id | client_first_name | client_last_name | client_date_of_birth |     client_occupation      
-----------+-------------------+------------------+----------------------+----------------------------
         1 | Kaiden            | Hill             |                 2006 | Student
         2 | Alina             | Morton           |                 2010 | Student
         3 | Fania             | Brooks           |                 1983 | Food Scientist
         4 | Courtney          | Jensen           |                 2006 | Student
         5 | Brittany          | Hill             |                 1983 | Firefighter
         6 | Max               | Rogers           |                 2005 | Student
         7 | Margaret          | McCarthy         |                 1981 | School Psychologist
         8 | Julie             | McCarthy         |                 1973 | Professor
         9 | Ken               | McCarthy         |                 1974 | Securities Clerk
        10 | Britany           | O'Quinn          |                 1984 | Violinist
        11 | Conner            | Gardner          |                 1998 | Licensed Massage Therapist
        12 | Mya               | Austin           |                 1960 | Parquet Floor Layer
        13 | Thierry           | Rogers           |                 2004 | Student
        14 | Eloise            | Rogers           |                 1984 | Computer Security Manager
        15 | Gerard            | Jackson          |                 1979 | Oil Exploration Engineer
        16 | Randy             | Day              |                 1986 | Aircraft Electrician
        17 | Jodie             | Page             |                 1990 | Manufacturing Director
        18 | Coral             | Rice             |                 1996 | Window Washer
        19 | Ayman             | Austin           |                 2002 | Student
        20 | Jaxson            | Austin           |                 1999 | Repair Worker
        21 | Joel              | Austin           |                 1973 | Police Officer
        22 | Alina             | Austin           |                 2010 | Student
        23 | Elin              | Austin           |                 1962 | Payroll Clerk
        24 | Ophelia           | Wolf             |                 2004 | Student
        25 | Eliot             | McGuire          |                 1967 | Dentist
        26 | Peter             | McKinney         |                 1968 | Professor
        27 | Annabella         | Henry            |                 1974 | Nurse
        28 | Anastasia         | Baker            |                 2001 | Student
        29 | Tyler             | Baker            |                 1984 | Police Officer
        30 | Lilian            | Ross             |                 1983 | Insurance Agent
        31 | Thierry           | Arnold           |                 1975 | Bus Driver
        32 | Angelina          | Rowe             |                 1979 | Firefighter
        33 | Marcia            | Rowe             |                 1974 | Health Educator
        34 | Martin            | Rowe             |                 1976 | Ship Engineer
        35 | Adeline           | Rowe             |                 2005 | Student
        36 | Colette           | Rowe             |                 1963 | Professor
        37 | Diane             | Clark            |                 1975 | Payroll Clerk
        38 | Caroline          | Clark            |                 1960 | Dentist
        39 | Dalton            | Clayton          |                 1982 | Police Officer
        40 | Steve             | Clayton          |                 1990 | Bus Driver
        41 | Melanie           | Clayton          |                 1987 | Computer Engineer
        42 | Alana             | Wilson           |                 2007 | Student
        43 | Carson            | Byrne            |                 1995 | Food Scientist
        44 | Conrad            | Byrne            |                 2007 | Student
        45 | Ryan              | Porter           |                 2008 | Student
        46 | Elin              | Porter           |                 1978 | Computer Programmer
        47 | Tyler             | Harvey           |                 2007 | Student
        48 | Arya              | Harvey           |                 2008 | Student
        49 | Serena            | Harvey           |                 1978 | School Teacher
        50 | Lilly             | Franklin         |                 1976 | Doctor
        51 | Mai               | Franklin         |                 1994 | Dentist
        52 | John              | Franklin         |                 1999 | Firefighter
        53 | Judy              | Franklin         |                 1995 | Firefighter
        54 | Katy              | Lloyd            |                 1992 | School Teacher
        55 | Tamara            | Allen            |                 1963 | Ship Engineer
        56 | Maxim             | Lyons            |                 1985 | Police Officer
        57 | Allan             | Lyons            |                 1983 | Computer Engineer
        58 | Marc              | Harris           |                 1980 | School Teacher
        59 | Elin              | Young            |                 2009 | Student
        60 | Diana             | Young            |                 2008 | Student
        61 | Diane             | Young            |                 2006 | Student
        62 | Alana             | Bird             |                 2003 | Student
        63 | Anna              | Becker           |                 1979 | Security Agent
        64 | Katie             | Grant            |                 1977 | Manager
        65 | Joan              | Grant            |                 2010 | Student
        66 | Bryan             | Bell             |                 2001 | Student
        67 | Belle             | Miller           |                 1970 | Professor
        68 | Peggy             | Stevens          |                 1990 | Bus Driver
        69 | Steve             | Williamson       |                 1975 | HR Clerk
        70 | Tyler             | Williamson       |                 1999 | Doctor
        71 | Izabelle          | Williamson       |                 1990 | Systems Analyst
        72 | Annabel           | Williamson       |                 1960 | Cashier
        73 | Mohamed           | Waters           |                 1966 | Insurance Agent
        74 | Marion            | Newman           |                 1970 | Computer Programmer
        75 | Ada               | Williams         |                 1986 | Computer Programmer
        76 | Sean              | Scott            |                 1983 | Bus Driver
        77 | Farrah            | Scott            |                 1974 | Ship Engineer
        78 | Christine         | Lambert          |                 1973 | School Teacher
        79 | Alysha            | Lambert          |                 2007 | Student
        80 | Maia              | Grant            |                 1984 | School Teacher


Question 2

client_first_name | client_last_name |       client_age        |     client_occupation      
-------------------+------------------+-------------------------+----------------------------
 Kaiden            | Hill             | 15 years 9 mons 20 days | Student
 Alina             | Morton           | 11 years 9 mons 20 days | Student
 Fania             | Brooks           | 38 years 9 mons 20 days | Food Scientist
 Courtney          | Jensen           | 15 years 9 mons 20 days | Student
 Brittany          | Hill             | 38 years 9 mons 20 days | Firefighter
 Max               | Rogers           | 16 years 9 mons 20 days | Student
 Margaret          | McCarthy         | 40 years 9 mons 20 days | School Psychologist
 Julie             | McCarthy         | 48 years 9 mons 20 days | Professor
 Ken               | McCarthy         | 47 years 9 mons 20 days | Securities Clerk
 Britany           | O'Quinn          | 37 years 9 mons 20 days | Violinist
 Conner            | Gardner          | 23 years 9 mons 20 days | Licensed Massage Therapist
 Mya               | Austin           | 61 years 9 mons 20 days | Parquet Floor Layer
 Thierry           | Rogers           | 17 years 9 mons 20 days | Student
 Eloise            | Rogers           | 37 years 9 mons 20 days | Computer Security Manager
 Gerard            | Jackson          | 42 years 9 mons 20 days | Oil Exploration Engineer
 Randy             | Day              | 35 years 9 mons 20 days | Aircraft Electrician
 Jodie             | Page             | 31 years 9 mons 20 days | Manufacturing Director
 Coral             | Rice             | 25 years 9 mons 20 days | Window Washer
 Ayman             | Austin           | 19 years 9 mons 20 days | Student
 Jaxson            | Austin           | 22 years 9 mons 20 days | Repair Worker
 Joel              | Austin           | 48 years 9 mons 20 days | Police Officer
 Alina             | Austin           | 11 years 9 mons 20 days | Student
 Elin              | Austin           | 59 years 9 mons 20 days | Payroll Clerk
 Ophelia           | Wolf             | 17 years 9 mons 20 days | Student
 Eliot             | McGuire          | 54 years 9 mons 20 days | Dentist
 Peter             | McKinney         | 53 years 9 mons 20 days | Professor
 Annabella         | Henry            | 47 years 9 mons 20 days | Nurse
 Anastasia         | Baker            | 20 years 9 mons 20 days | Student
 Tyler             | Baker            | 37 years 9 mons 20 days | Police Officer
 Lilian            | Ross             | 38 years 9 mons 20 days | Insurance Agent
 Thierry           | Arnold           | 46 years 9 mons 20 days | Bus Driver
 Angelina          | Rowe             | 42 years 9 mons 20 days | Firefighter
 Marcia            | Rowe             | 47 years 9 mons 20 days | Health Educator
 Martin            | Rowe             | 45 years 9 mons 20 days | Ship Engineer
 Adeline           | Rowe             | 16 years 9 mons 20 days | Student
 Colette           | Rowe             | 58 years 9 mons 20 days | Professor
 Diane             | Clark            | 46 years 9 mons 20 days | Payroll Clerk
 Caroline          | Clark            | 61 years 9 mons 20 days | Dentist
 Dalton            | Clayton          | 39 years 9 mons 20 days | Police Officer
 Steve             | Clayton          | 31 years 9 mons 20 days | Bus Driver
 Melanie           | Clayton          | 34 years 9 mons 20 days | Computer Engineer
 Alana             | Wilson           | 14 years 9 mons 20 days | Student
 Carson            | Byrne            | 26 years 9 mons 20 days | Food Scientist
 Conrad            | Byrne            | 14 years 9 mons 20 days | Student
 Ryan              | Porter           | 13 years 9 mons 20 days | Student
 Elin              | Porter           | 43 years 9 mons 20 days | Computer Programmer
 Tyler             | Harvey           | 14 years 9 mons 20 days | Student
 Arya              | Harvey           | 13 years 9 mons 20 days | Student
 Serena            | Harvey           | 43 years 9 mons 20 days | School Teacher
 Lilly             | Franklin         | 45 years 9 mons 20 days | Doctor
 Mai               | Franklin         | 27 years 9 mons 20 days | Dentist
 John              | Franklin         | 22 years 9 mons 20 days | Firefighter
 Judy              | Franklin         | 26 years 9 mons 20 days | Firefighter
 Katy              | Lloyd            | 29 years 9 mons 20 days | School Teacher
 Tamara            | Allen            | 58 years 9 mons 20 days | Ship Engineer
 Maxim             | Lyons            | 36 years 9 mons 20 days | Police Officer
 Allan             | Lyons            | 38 years 9 mons 20 days | Computer Engineer
 Marc              | Harris           | 41 years 9 mons 20 days | School Teacher
 Elin              | Young            | 12 years 9 mons 20 days | Student
 Diana             | Young            | 13 years 9 mons 20 days | Student
 Diane             | Young            | 15 years 9 mons 20 days | Student
 Alana             | Bird             | 18 years 9 mons 20 days | Student
 Anna              | Becker           | 42 years 9 mons 20 days | Security Agent
 Katie             | Grant            | 44 years 9 mons 20 days | Manager
 Joan              | Grant            | 11 years 9 mons 20 days | Student
 Bryan             | Bell             | 20 years 9 mons 20 days | Student
 Belle             | Miller           | 51 years 9 mons 20 days | Professor
 Peggy             | Stevens          | 31 years 9 mons 20 days | Bus Driver
 Steve             | Williamson       | 46 years 9 mons 20 days | HR Clerk
 Tyler             | Williamson       | 22 years 9 mons 20 days | Doctor
 Izabelle          | Williamson       | 31 years 9 mons 20 days | Systems Analyst
 Annabel           | Williamson       | 61 years 9 mons 20 days | Cashier
 Mohamed           | Waters           | 55 years 9 mons 20 days | Insurance Agent
 Marion            | Newman           | 51 years 9 mons 20 days | Computer Programmer
 Ada               | Williams         | 35 years 9 mons 20 days | Computer Programmer
 Sean              | Scott            | 38 years 9 mons 20 days | Bus Driver
 Farrah            | Scott            | 47 years 9 mons 20 days | Ship Engineer
 Christine         | Lambert          | 48 years 9 mons 20 days | School Teacher
 Alysha            | Lambert          | 14 years 9 mons 20 days | Student
 Maia              | Grant            | 37 years 9 mons 20 days | School Teacher


Question 3

 client_first_name | client_last_name 
-------------------+------------------
 Maia              | Grant 
 Marcia            | Rowe 
 Alysha            | Lambert 
 Tyler             | Baker 
 Katy              | Lloyd 
 Angelina          | Rowe 
 Gerard            | Jackson 
 Carson            | Byrne

Question 4

 author_first_name | author_last_name | book_id | count 
-------------------+------------------+---------+-------
 Logan             | Moore            |      13 |     7
 Elena             | Martin           |       3 |     6
 Zoe               | Roy              |       4 |     5
 Isabelle          | Lee              |      18 |     4
 Oliver            | Martin           |      15 |     4

Question 5

 author_nationality 
--------------------
 France
 China
 Spain
 Great Britain
 USA

Question 6

           book_title            | book_id | count 
---------------------------------+---------+-------
 The perfect match               |       3 |    13
 Electrical transformers         |      13 |    12
 Performance evaluation          |      18 |    11
 Positive figures                |      32 |    11
 Green nature                    |      22 |    11
 Right and left                  |      15 |    10
 Programming using Python        |      16 |    10
 Industrial revolution           |      21 |    10
 Digital Logic                   |       4 |    10
 Director and leader             |      25 |     9
 The red wall                    |       2 |     9
 Computer networks               |      17 |     9
 A gray park                     |       8 |     9
 Build your database system      |       1 |     9
 How to be rich in one year      |       9 |     8
 How to be a great lawyer        |       5 |     8
 Perfect rugby                   |      28 |     8
 The silver uniform              |      20 |     7
 Pollution today                 |       7 |     7
 Computer security               |      30 |     7
 The chocolate love              |      24 |     7
 Daily exercise                  |      19 |     6
 Their bright fate               |      10 |     6
 History of theater              |      12 |     6
 Black lines                     |      11 |     5
 The end                         |      29 |     5
 Build your big data system      |      14 |     5
 Perfect football                |      23 |     5
 Participate                     |      31 |     4
 Play football every week        |      26 |     4
 Maya the bee                    |      27 |     4
 Manage successful negotiations  |       6 |     3

Question 7

   genre    | count 
------------+-------
 Science    |    24
 Fiction    |    16
 Well being |    15
 Humor      |     5
 Society    |     4
 Children   |     3
 History    |     3
 Literature |     3
 Law        |     3

Question 8

  client_occupation  | count 
---------------------+-------
 Student             |    32
 Bus Driver          |     8
 Computer Programmer |     6
 Dentist             |     6
 Firefighter         |     5

Question 9

     client_occupation      | average 
----------------------------+---------
 Student                    |    4.42
 School Teacher             |    3.60
 Firefighter                |    3.25
 Professor                  |    3.50
 Bus Driver                 |    4.00
 Police Officer             |    4.50
 Dentist                    |    5.67
 Computer Programmer        |    5.67
 Ship Engineer              |    4.00
 Payroll Clerk              |    3.00
 Computer Engineer          |    3.00
 Insurance Agent            |    4.00
 Doctor                     |    4.00
 Manufacturing Director     |    5.00
 Computer Security Manager  |    6.00
 Security Agent             |    2.00
 Cashier                    |    5.00
 Aircraft Electrician       |    2.00
 School Psychologist        |    2.00
 Repair Worker              |    3.00
 Systems Analyst            |    4.00
 Manager                    |    3.00
 Food Scientist             |    5.00
 Nurse                      |    7.00
 Parquet Floor Layer        |    2.00
 Health Educator            |    2.00
 HR Clerk                   |    4.00
 Licensed Massage Therapist |    2.00
 Securities Clerk           |    2.00
 Violinist                  |    4.00
 Window Washer              |    2.00
 Oil Exploration Engineer   |    5.00

Question 10

 book_id |        book_title        | count | twenty_percent 
---------+--------------------------+-------+----------------
      13 | Electrical transformers  |    18 |             16

Question 11

   month   | count 
-----------+-------
 August    |    10
 July      |    10
 October   |    10
 December  |     7
 April     |     7
 June      |     6
 February  |     6
 September |     6
 January   |     5
 May       |     5
 March     |     4

Question 12

 average | client_date_of_birth 
---------+----------------------
    3.67 |                 1960
    3.00 |                 1962
    5.00 |                 1963
    1.00 |                 1966
    3.00 |                 1967
    4.00 |                 1968
    4.50 |                 1970
    3.67 |                 1973
    3.25 |                 1974
    2.67 |                 1975
    3.50 |                 1976
    3.00 |                 1977
    5.50 |                 1978
    4.33 |                 1979
    1.00 |                 1980
    2.00 |                 1981
    3.00 |                 1982
    3.75 |                 1983
    5.50 |                 1984
    4.00 |                 1985
    3.00 |                 1986
    2.00 |                 1987
    5.50 |                 1990
    3.00 |                 1992
   10.00 |                 1994
    4.50 |                 1995
    2.00 |                 1996
    2.00 |                 1998
    3.67 |                 1999
    4.50 |                 2001
    2.00 |                 2002
    5.00 |                 2003
    3.00 |                 2004
    4.50 |                 2005
    5.50 |                 2006
    5.00 |                 2007
    6.00 |                 2008
    2.33 |                 2010

Question 13

Oldest client:

 client_id | client_first_name | client_last_name | client_date_of_birth |  client_occupation  
-----------+-------------------+------------------+----------------------+---------------------
        12 | Mya               | Austin           |                 1960 | Parquet Floor Layer

Youngest client:

 client_id | client_first_name | client_last_name | client_date_of_birth | client_occupation 
-----------+-------------------+------------------+----------------------+-------------------
         2 | Alina             | Morton           |                 2010 | Student

Question 14

 count | book_author | author_first_name | author_last_name 
-------+-------------+-------------------+------------------
(0 rows)
